class riseuphelp::amber {

  include site_ruby::bundler

  vcsrepo { '/srv/amber':
    ensure   => latest,
    provider => git,
    force    => true,
    revision => 'master',
    source   => 'https://github.com/leapcode/amber',
    owner    => root,
    group    => root,
    require  => Class['git'],
    notify   => Exec['amber_bundler_update']
  }

  exec { 'amber_bundler_update':
    cwd         => '/srv/amber',
    environment => 'LANG=en_US.UTF-8',
    command     => '/bin/bash -c "/usr/bin/bundle check || /usr/bin/bundle install --without test development"',
    unless      => '/usr/bin/bundle check',
    user        => root,
    timeout     => 600,
    require     => [
                    Class['bundler::install'],
                    Vcsrepo['/srv/amber'],
                    Class['site_ruby::bundler'] ],
    notify      => Service['apache'];
  }

}
