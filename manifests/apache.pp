class riseuphelp::apache {

  include site_apache::wildcard

  apache::module {
    'headers': ensure => present;
    'rewrite': ensure => present;
    'removeip': ensure => present, package_name => 'libapache2-mod-removeip';
  }

  apache::vhost::file {
    'help.riseup.net':
      content => template('riseupsites/lyre/help.riseup.net');
  }
}
