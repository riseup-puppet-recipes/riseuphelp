class riseuphelp::pages {

  vcsrepo { '/srv/help':
    ensure   => latest,
    provider => git,
    force    => true,
    revision => 'master',
    source   => 'https://github.com/riseupnet/riseup_help',
    owner    => www-data,
    group    => www-data,
    require  => [ Class['git'], Class['apache'], Class['riseuphelp::amber'] ],
    notify   => Exec['amber_rebuild']
  }

  exec { 'amber_rebuild':
    cwd       => '/srv/help',
    command   => '/srv/amber/bin/amber rebuild',
    user      => 'www-data',
    group     => 'www-data',
    logoutput => 'on_failure',
    timeout   => 600,
    require   => [ Class['riseuphelp::amber'], Vcsrepo['/srv/help'] ]
  }
}
